# 介绍

1. 使用前点击 docker-compose.yml 中的绿色按钮进行启动
2. IDEA 启动失败可参考 https://blog.csdn.net/weixin_63594548/article/details/140962044
   或在 docker-compose.yml 文件所属目录执行 `docker-compose up`
3. 看板需要手动加载,具体参考如下
4. doc/prometheus/etc/prometheus/prometheus.yml 中的 scrape_configs.static_configs
   .targets 中的 IP 地址不能是 localhost 不能是 127.0.0.1 不能是 0.0.0.0，需要主机上 19 开头的
   IP 地址，可使用 ifconfig 进行查询

## 如何加载看板

1. 访问 http://localhost:3000 打开 grafana 输入 admin、admin 账号和密码
   ![img.png](./doc/png/img.png)
2. 点击 Home -> Dashboards -> New -> Import
   ![img_1.png](./doc/png/img_1.png)
3. 将 etc/grafana/provisioning/dashboards 中的 JSON 内容复制到 Import via panel json 后点击 Load
   ![img_2.png](./doc/png/img_2.png)
4. 点击 Import
   ![img_3.png](./doc/png/img_3.png)

## 如何汉化

参考：https://blog.csdn.net/weixin_63594548/article/details/142178195

## 如何监控多台数据库

创建多个 exporter 实例，让 Prometheus 统一监控 
